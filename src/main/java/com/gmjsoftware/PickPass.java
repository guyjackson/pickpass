package com.gmjsoftware;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PickPass {
    public Map<Integer, Character> findValues(String password, List<Integer> indexes) {

        validate(password, indexes);

        var passwordChars = password.toCharArray();

        var result = new HashMap<Integer, Character>();

        for (var index :
                indexes) {
            var passwordChar = passwordChars[index - 1];
            result.put(index, passwordChar);
        }

        return result;
    }

    private void validate(String password, List<Integer> indexes) {
        if (password == null || password.length() < 1)
            throw new IllegalArgumentException("Password must have a minimum length of one");
        if (indexes == null || indexes.isEmpty())
            throw new IllegalArgumentException("Indexes must have a minimum size of one");

        if (indexes.stream()
                .anyMatch(i -> i > password.length()))
            throw new IllegalArgumentException("Indexes must not be greater than password length");

    }
}
