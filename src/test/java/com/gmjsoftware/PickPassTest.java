package com.gmjsoftware;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class PickPassTest {

    private PickPass pickPass;

    @BeforeEach
    void setUp() {
        pickPass = new PickPass();
    }

    @Test
    void password_must_have_a_minimum_length_of_one() {
        var password = "";
        var indexes = Arrays.asList(1);

        assertThatThrownBy(() -> pickPass.findValues(password, indexes))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Password must have a minimum length of one");
    }

    @Test
    void indexes_must_have_a_minimum_size_of_one() {
        var password = "abc";
        List<Integer> indexes = new ArrayList<>();

        assertThatThrownBy(() -> pickPass.findValues(password, indexes))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Indexes must have a minimum size of one");
    }

    @Test
    void indexes_must_not_be_greater_than_password_length() {
        var password = "four";
        var indexes = Arrays.asList(5);

        assertThatThrownBy(() -> pickPass.findValues(password, indexes))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Indexes must not be greater than password length");
    }

    @Test
    void password_character_identified_from_index() {
        var password = "aPassword";
        var indexes = Arrays.asList(1,2,9);

        var passwordCharacters = pickPass.findValues(password, indexes);

        assertThat(passwordCharacters.get(1)).isEqualTo('a');
        assertThat(passwordCharacters.get(2)).isEqualTo('P');
        assertThat(passwordCharacters.get(9)).isEqualTo('d');
    }
}
